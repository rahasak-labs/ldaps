package com.rahasak.ldaps

import java.util.Properties
import javax.naming.Context
import javax.naming.directory.InitialDirContext
import scala.util.{Failure, Success, Try}


object LdapAuth extends App {

  def authUser(uid: String, password: String): Boolean = {
    Try {
      val props = new Properties()
      props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory")
      props.put(Context.PROVIDER_URL, "ldap://192.168.64.25:389")
      props.put(Context.SECURITY_PRINCIPAL, s"uid=$uid,dc=rahasak,dc=com")
      props.put(Context.SECURITY_CREDENTIALS, password)

      new InitialDirContext(props)
    } match {
      case Success(_) => true
      case Failure(_) => false
    }
  }

  println(authUser("bassa", "abc"))
}
